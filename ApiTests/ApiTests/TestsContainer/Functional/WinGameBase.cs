﻿using System;
using System.Threading.Tasks;
using ApiTests.TestsCommon;
using FluentAssertions;
using NUnit.Framework;

namespace ApiTests.TestsContainer.Functional
{
    public class WinGameBase : TestBase
    {
        protected const int Level1BuildingCount = 27;
        protected const int Level2BuildingCount = 9;
        protected const int Level3BuildingCount = 3;
        protected const int Level4BuildingCount = 1;

        protected readonly int _rawResouceNeeded = Convert.ToInt32(ConfigurationProvider.VictoryTopResourceAmount * Math.Pow(ConfigurationProvider.LowerResourcesNeededForHigher, ConfigurationProvider.FactoryLevels));

        protected async Task PrepareStonesForFirstMineAsync(Client player, int timeoutMs)
        {
            var stoneTile = await player.TryFindFirstFreeResourceAsync(ResourceType.Stone);
            stoneTile.Should().NotBeNull();

            for (var i = 0; i < ConfigurationProvider.StoneNeededForBuilding; i++)
            {
                var mineResult = await player.TryMineAsync(stoneTile.X, stoneTile.Y);
                mineResult.Should().BeTrue();
            }

            var stoneIsReady = await player.WaitForResourceAsync(ResourceType.Stone, ConfigurationProvider.StoneNeededForBuilding, timeoutMs);
            stoneIsReady.Should().BeTrue();
        }

        protected async Task MakeRawResourceGoingAsync(Client player, string resource, int oneMineTimeoutMs)
        {
            var resourceGained = 0;
            var minesBuilt = 0;
            var resourceNeeded = _rawResouceNeeded;

            if (resource == ResourceType.Stone)
                resourceNeeded = await GetNumberOfStoneNeeded(player);

            if (resource == ResourceType.Coal)
                resourceNeeded = GetNumberOfCoalNeeded();

            var numberOfResourceTilesOnMap = await player.GetNumberOfResourceDepositsOnMapAsync(resource);
            numberOfResourceTilesOnMap.Should().BeGreaterThan(0);

            while (resourceGained < resourceNeeded)
            {
                if (minesBuilt == numberOfResourceTilesOnMap)
                    Assert.Fail($"There is not enough resource deposits of '{resource}' to finish the game, expected '{resourceNeeded}' but found only '{resourceGained}'");

                var stoneIsReady = await player.WaitForResourceAsync(ResourceType.Stone, ConfigurationProvider.StoneNeededForBuilding, oneMineTimeoutMs);
                stoneIsReady.Should().BeTrue();

                var rawResourceCoords = await player.TryFindFirstFreeResourceAsync(resource);
                rawResourceCoords.Should().NotBeNull();

                var map = await player.GetMapAsync();
                var rawResourceValue = map.Tiles[rawResourceCoords.Y, rawResourceCoords.X].ResourceSite.Amount;

                var mineIsBuild = await player.TryBuildAsync(BuildingType.Mine, rawResourceCoords.X, rawResourceCoords.Y);
                mineIsBuild.Should().BeTrue();

                var mineIsReady = await player.WaitForBuildingAsync(BuildingType.Mine, rawResourceCoords.X, rawResourceCoords.Y, oneMineTimeoutMs);
                mineIsReady.Should().BeTrue();

                resourceGained += rawResourceValue;
                minesBuilt++;
            }
        }

        int GetNumberOfCoalNeeded()
        {
            var margin = 500; // reserve because i will always mine higher amount of resources than needed
            int coalNeeded = ConfigurationProvider.VictoryTopResourceAmount;
            int coalNeededThisLevel = coalNeeded;

            for (var i = 0; i < ConfigurationProvider.FactoryLevels - 1; i++)
            {
                coalNeededThisLevel = coalNeededThisLevel * ConfigurationProvider.LowerResourcesNeededForHigher;
                coalNeeded += coalNeededThisLevel;
            }

            return coalNeeded * ConfigurationProvider.NumberOfProductionResources + margin;
        }

        async Task<int> GetNumberOfStoneNeeded(Client player)
        {
            var factories = Level1BuildingCount + Level2BuildingCount + Level3BuildingCount + Level4BuildingCount;
            var mines = 0;

            foreach (var resource in ResourceCategory.RawResources)
            {
                mines += await player.GetNumberOfResourceDepositsOnMapAsync(resource);
            }

            return (factories + mines) * ConfigurationProvider.StoneNeededForBuilding;
        }

        protected async Task WaitForVictoryAsync(Client player)
        {
            var haveAllRocketShells = await player.WaitForResourceAsync(ResourceType.RocketShell, 100, (int)TimeSpan.FromMinutes(5).TotalMilliseconds);
            var haveAllComputers = await player.WaitForResourceAsync(ResourceType.Computer, 100, (int)TimeSpan.FromMinutes(5).TotalMilliseconds);
            var haveAllRocketFuel = await player.WaitForResourceAsync(ResourceType.RocketFuel, 100, (int)TimeSpan.FromMinutes(5).TotalMilliseconds);

            haveAllRocketShells.Should().BeTrue();
            haveAllComputers.Should().BeTrue();
            haveAllRocketFuel.Should().BeTrue();
        }

        async Task<TileCoords> GetTileAndSendBuildRequestAsync(Client player, string factory, bool multipleClients)
        {
            var buildingTile = multipleClients
                ? await player.TryFindRandomFreeResourceAsync(ResourceType.None)
                : await player.TryFindFirstFreeResourceAsync(ResourceType.None);
            buildingTile.Should().NotBeNull();

            var factoryIsBuilt = await player.TryBuildAsync(factory, buildingTile.X, buildingTile.Y);
            factoryIsBuilt.Should().BeTrue();

            return buildingTile;
        }

        protected async Task MakeFactoryGoingAsync(Client player, string factory, int numberOfFactories, bool multipleClients, int oneFactoryTimeout)
        {
            for (var i = 0; i < numberOfFactories; i++)
            {
                var stoneIsReady = await player.WaitForResourceAsync(ResourceType.Stone, ConfigurationProvider.StoneNeededForBuilding, oneFactoryTimeout);
                stoneIsReady.Should().BeTrue();

                var buildingTile = await GetTileAndSendBuildRequestAsync(player, factory, multipleClients);

                var factoryIsReady = await player.WaitForBuildingAsync(factory, buildingTile.X, buildingTile.Y, oneFactoryTimeout);

                if (multipleClients)
                {
                    if (!factoryIsReady)
                    {
                        var retryFactoryIsReadySuccessful = await RetryBuildAsync(player, factory, multipleClients, oneFactoryTimeout);
                        retryFactoryIsReadySuccessful.Should().BeTrue();
                    }
                }
                else
                {
                    factoryIsReady.Should().BeTrue();
                }
            }
        }

        async Task<bool> RetryBuildAsync(Client player, string factory, bool multipleClients, int timeoutMs) // in case multiple clients will try to build on same tile
        {
            TestLogger.WriteLineT($"Unable to build '{factory}' for player '{player}', will try again on different location", LogSeverity.Trace);

            var maxRetryAttempts = 100;
            var retryCounter = 0;

            while (retryCounter < maxRetryAttempts)
            {
                var buildingTile = await GetTileAndSendBuildRequestAsync(player, factory, multipleClients);
                var factoryIsReady = await player.WaitForBuildingAsync(factory, buildingTile.X, buildingTile.Y, timeoutMs);

                if (factoryIsReady)
                {
                    TestLogger.WriteLineT($"Retry building of '{factory}' for player '{player}' successful at '[{buildingTile.X},{buildingTile.Y}]'", LogSeverity.Trace);
                    return true;
                }
            }

            TestLogger.WriteLineT($"Unable to build '{factory}' for player '{player}' after 100th retry", LogSeverity.Warn);
            return false;
        }
    }
}
