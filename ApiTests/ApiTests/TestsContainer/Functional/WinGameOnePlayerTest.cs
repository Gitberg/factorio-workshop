﻿using System.Linq;
using System.Threading.Tasks;
using ApiTests.TestsCommon;
using NUnit.Framework;

namespace ApiTests.TestsContainer.Functional
{
    [TestFixture]
    class WinGameOnePlayerTest : WinGameBase
    {
        [Test]
        public async Task RunWinGameOnePlayerTestAsync()
        {
            var player = new Client(ConfigurationProvider.PlayerNames.First());
            var defaultBuildTimeoutMs = ConfigurationProvider.TickTimeMs * ConfigurationProvider.StoneNeededForBuilding;

            TestLogger.WriteLineT("Mining stones for first mine", LogSeverity.Info);
            await PrepareStonesForFirstMineAsync(player, defaultBuildTimeoutMs + ConfigurationProvider.TickTimeMs);

            TestLogger.WriteLineT("Building mines on raw material deposits", LogSeverity.Info);
            await MakeRawResourceGoingAsync(player, ResourceType.Stone, defaultBuildTimeoutMs);
            await MakeRawResourceGoingAsync(player, ResourceType.Coal, defaultBuildTimeoutMs);
            await MakeRawResourceGoingAsync(player, ResourceType.IronOre, defaultBuildTimeoutMs);
            await MakeRawResourceGoingAsync(player, ResourceType.CopperOre, defaultBuildTimeoutMs);
            await MakeRawResourceGoingAsync(player, ResourceType.Oil, defaultBuildTimeoutMs);

            TestLogger.WriteLineT("Building level 1 factories", LogSeverity.Info);
            await MakeFactoryGoingAsync(player, BuildingType.IronPlateFactory, Level1BuildingCount, false, defaultBuildTimeoutMs);
            await MakeFactoryGoingAsync(player, BuildingType.CopperPlateFactory, Level1BuildingCount, false, defaultBuildTimeoutMs);
            await MakeFactoryGoingAsync(player, BuildingType.PetroleumFactory, Level1BuildingCount, false, defaultBuildTimeoutMs);

            TestLogger.WriteLineT("Building level 2 factories", LogSeverity.Info);
            await MakeFactoryGoingAsync(player, BuildingType.SteelFactory, Level2BuildingCount, false, defaultBuildTimeoutMs);
            await MakeFactoryGoingAsync(player, BuildingType.WireFactory, Level2BuildingCount, false, defaultBuildTimeoutMs);
            await MakeFactoryGoingAsync(player, BuildingType.BasicFuelFactory, Level2BuildingCount, false, defaultBuildTimeoutMs);

            TestLogger.WriteLineT("Building level 3 factories", LogSeverity.Info);
            await MakeFactoryGoingAsync(player, BuildingType.SteelCasingFactory, Level3BuildingCount, false, defaultBuildTimeoutMs);
            await MakeFactoryGoingAsync(player, BuildingType.CircuitFactory, Level3BuildingCount, false, defaultBuildTimeoutMs);
            await MakeFactoryGoingAsync(player, BuildingType.SolidFuelFactory, Level3BuildingCount, false, defaultBuildTimeoutMs);

            TestLogger.WriteLineT("Building level 4 factories", LogSeverity.Info);
            await MakeFactoryGoingAsync(player, BuildingType.RocketShellFactory, Level4BuildingCount, false, defaultBuildTimeoutMs);
            await MakeFactoryGoingAsync(player, BuildingType.ComputerFactory, Level4BuildingCount, false, defaultBuildTimeoutMs);
            await MakeFactoryGoingAsync(player, BuildingType.RocketFuelFactory, Level4BuildingCount, false, defaultBuildTimeoutMs);

            TestLogger.WriteLineT($"Waiting for '{ConfigurationProvider.VictoryTopResourceAmount}' pieces of all tier 4 resources", LogSeverity.Info);
            await WaitForVictoryAsync(player);
        }
    }
}
