﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApiTests.TestsCommon;
using FluentAssertions;
using NUnit.Framework;

namespace ApiTests.TestsContainer.Functional
{
    [TestFixture]
    class WinGameMultiplePlayersTest : WinGameBase
    {
        [Test]
        public async Task RunWinGameMultiplePlayersTest()
        {
            var stoneWorker = new Client(ConfigurationProvider.PlayerNames[0]);
            var coalWorker = new Client(ConfigurationProvider.PlayerNames[1]);
            var ironWorker = new Client(ConfigurationProvider.PlayerNames[2]);
            var copperWorker = new Client(ConfigurationProvider.PlayerNames[3]);
            var oilWorker = new Client(ConfigurationProvider.PlayerNames[4]);
            var defaultBuildTimeoutMs = ConfigurationProvider.TickTimeMs * ConfigurationProvider.StoneNeededForBuilding;

            TestLogger.WriteLineT("Mining stones for first mine", LogSeverity.Info);
            await PrepareStonesForFirstMineAsync(stoneWorker, defaultBuildTimeoutMs + ConfigurationProvider.TickTimeMs);

            TestLogger.WriteLineT("Building mines on stone deposits", LogSeverity.Info);
            await ManageRawResourceAsync(coalWorker, ResourceType.Stone, defaultBuildTimeoutMs);

            TestLogger.WriteLineT("Releasing multiple clients to take care about rest of resources and building together", LogSeverity.Info);
            var taskList = new List<Task>();
            taskList.Add(Task.Run(() => ManageRawResourceAsync(coalWorker, ResourceType.Coal, defaultBuildTimeoutMs)));
            taskList.Add(Task.Run(() => ManageProductionResourceAsync(ironWorker, ResourceType.IronOre, BuildingCategory.IronBuildings, defaultBuildTimeoutMs)));
            taskList.Add(Task.Run(() => ManageProductionResourceAsync(copperWorker, ResourceType.CopperOre, BuildingCategory.CopperBuildings, defaultBuildTimeoutMs)));
            taskList.Add(Task.Run(() => ManageProductionResourceAsync(oilWorker, ResourceType.Oil, BuildingCategory.OilBuildings, defaultBuildTimeoutMs)));
            await Task.WhenAll(taskList);

            TestLogger.WriteLineT($"Waiting for '{ConfigurationProvider.VictoryTopResourceAmount}' pieces of all tier 4 resources", LogSeverity.Info);
            await WaitForVictoryAsync(stoneWorker);
        }

        async Task ManageRawResourceAsync(Client player, string rawResource, int timeoutMs)
        {
            await MakeRawResourceGoingAsync(player, rawResource, timeoutMs);
        }

        async Task ManageProductionResourceAsync(Client player, string rawResource, List<string> buildingList, int timeoutMs)
        {
            await MakeRawResourceGoingAsync(player, rawResource, timeoutMs);

            buildingList.Count.Should().Be(ConfigurationProvider.FactoryLevels);
            await MakeFactoryGoingAsync(player, buildingList[0], Level1BuildingCount, true, timeoutMs);
            await MakeFactoryGoingAsync(player, buildingList[1], Level2BuildingCount, true, timeoutMs);
            await MakeFactoryGoingAsync(player, buildingList[2], Level3BuildingCount, true, timeoutMs);
            await MakeFactoryGoingAsync(player, buildingList[3], Level4BuildingCount, true, timeoutMs);
        }
    }
}
