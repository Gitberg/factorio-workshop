﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ApiTests.TestsCommon;
using FluentAssertions;
using NUnit.Framework;

namespace ApiTests.TestContainer.Smoke
{
    [TestFixture]
    public class BuildStoneReserveTest : TestBase
    {
        readonly int _woldTickTimeMs = ConfigurationProvider.TickTimeMs;
        readonly int _resourcesPerMineTick = ConfigurationProvider.ResourceGainFromMineTick;
        readonly int _stoneNeededForMine = ConfigurationProvider.StoneNeededForBuilding;

        [Test]
        public async Task RunBuildStoneReserveTestAsync()
        {
            var stoneReserveGoal = 16;
            var player = new Client(ConfigurationProvider.PlayerNames.First());

            TestLogger.WriteLineT("Looking for first stone deposit", LogSeverity.Info);
            var tileToMine = await player.TryFindFirstFreeResourceAsync(ResourceType.Stone);
            tileToMine.Should().NotBeNull();

            TestLogger.WriteLineT("Mining first stone for a mine", LogSeverity.Info);
            for (var i = 0; i < _stoneNeededForMine; i++)
            {
                var mineResult = await player.TryMineAsync(tileToMine.X, tileToMine.Y);
                mineResult.Should().BeTrue();
            }

            var stonesAreReadyTimeoutMs = _woldTickTimeMs * (_stoneNeededForMine + 1);
            var stonesAreReady = await player.WaitForResourceAsync(ResourceType.Stone, _stoneNeededForMine, stonesAreReadyTimeoutMs);
            stonesAreReady.Should().BeTrue();

            TestLogger.WriteLineT("Building a mine on first stone deposit and waiting until goal of stone mined is reached", LogSeverity.Info);
            var mineIsBuild = await player.TryBuildAsync(BuildingType.Mine, tileToMine.X, tileToMine.Y);
            mineIsBuild.Should().BeTrue();

            var stoneReserveTimeoutMs = (int)Math.Ceiling((double)(_woldTickTimeMs * (stoneReserveGoal + _resourcesPerMineTick) / _resourcesPerMineTick));
            var stoneReserveIsReady = await player.WaitForResourceAsync(ResourceType.Stone, stoneReserveGoal, stoneReserveTimeoutMs);
            stoneReserveIsReady.Should().BeTrue();

            TestLogger.WriteLineT("Destroying the mine", LogSeverity.Info);
            var mineIsDestroyed = await player.TryDestroyAsync(tileToMine.X, tileToMine.Y);
            mineIsDestroyed.Should().BeTrue();

            TestLogger.WriteLineT("Checking that no stone is gained when mine is destroyed", LogSeverity.Info);
            await Task.Delay(TimeSpan.FromMilliseconds(_woldTickTimeMs * 5));

            var resourcesAvailable = await player.GetResourcesAsync();
            resourcesAvailable.First(r => r.Resource == ResourceType.Stone).Amount.Should().BeInRange(stoneReserveGoal, stoneReserveGoal + 2 * _resourcesPerMineTick);
        }
    }
}
