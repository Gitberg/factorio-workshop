﻿using System.Linq;
using System.Threading.Tasks;
using ApiTests.TestsCommon;
using FluentAssertions;
using NUnit.Framework;

namespace ApiTests.TestContainer.Smoke
{
    [TestFixture]
    public class GetResourcesTest : TestBase
    {       
        [Test]
        public async Task RunGetResourcesTestAsync()
        {
            TestLogger.WriteLineT("Getting player resource pool", LogSeverity.Info);
            var player = new Client(ConfigurationProvider.PlayerNames.First());
            var resourceList = ResourceCategory.AllResources;
            var resourceData = await player.GetResourcesAsync();
            resourceData.Should().NotBeNull();
            resourceData.Count.Should().Be(resourceList.Count());

            TestLogger.WriteLineT("Checking that resource pool and known resources match together", LogSeverity.Info);
            foreach (var resourceType in resourceList)
            {
                resourceData.Select(r => r.Resource).Intersect(resourceList).Any().Should().BeTrue();
            }
        }
    }
}
