﻿using System.Linq;
using System.Threading.Tasks;
using ApiTests.TestsCommon;
using FluentAssertions;
using NUnit.Framework;

namespace ApiTests.TestContainer.Smoke
{
    [TestFixture]
    public class GetMapTest : TestBase
    {
        [Test]
        public async Task RunGetMapTestAsync()
        {
            TestLogger.WriteLineT("Getting map data", LogSeverity.Info);
            var player = new Client(ConfigurationProvider.PlayerNames.First());
            var mapData = await player.GetMapAsync();
            mapData.Should().NotBeNull();
            mapData.Tiles.GetLength(0).Should().Be(ConfigurationProvider.MapHeight);
            mapData.Tiles.GetLength(1).Should().Be(ConfigurationProvider.MapWidth);

            TestLogger.WriteLineT("Trying to find at least one deposit per each known resource inside map data", LogSeverity.Info);
            foreach (var resource in ResourceCategory.RawResources)
            {
                var tile = await player.TryFindFirstFreeResourceAsync(resource);
                tile.Should().NotBeNull();
            }
        }
    }
}
