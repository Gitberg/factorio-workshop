﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;

namespace ApiTests.TestsCommon
{
    public class ProcessHandler
    {
        readonly string _beExePath;
        readonly string _beExeName;
        readonly string _wsBatPath;
        readonly string _wsBatName;
        readonly string _wsWebPath;

        Process _backendProcess;
        Process _webServerProcess;

        public ProcessHandler()
        {
            _beExePath = ConfigurationProvider.BackendExecutableFilePath;
            _beExeName = ConfigurationProvider.BackendExecutableFileName;
            _wsBatPath = ConfigurationProvider.WebServerExecutablePath;
            _wsBatName = ConfigurationProvider.WebServerExecutableFileName;
            _wsWebPath = ConfigurationProvider.WebServerLocalhostPath;

            _backendProcess = new Process();
            _webServerProcess = new Process();
        }

        async Task WaitForProcessIsReady(string processName, int timeoutMs, int delayBetweenChecksMs, LogSeverity severity)
        {
            var currentTime = DateTime.UtcNow;
            var timeoutTime = DateTime.UtcNow.AddMilliseconds(timeoutMs);

            while (currentTime <= timeoutTime)
            {
                var process = Process.GetProcessesByName(processName).FirstOrDefault();

                if (process != null)
                    return;

                await Task.Delay(TimeSpan.FromMilliseconds(delayBetweenChecksMs));
                currentTime = DateTime.UtcNow;
            }

            var errorMessage = $"Process '{processName}' was not start in given time limit '{timeoutMs}' ms";
            TestLogger.WriteLineT(errorMessage, severity);
            throw new Exception(errorMessage);
        }

        void MakeSureNoProcessIsRunning(string processName, LogSeverity severity)
        {
            foreach (var process in Process.GetProcessesByName(processName))
            {
                try
                {
                    process.Kill();
                    TestLogger.WriteLineT($"Process '{processName}' with PID '{process.Id}' has been killed", LogSeverity.Trace);
                }
                catch (Exception ex)
                {
                    TestLogger.WriteLineT($"Unable to kill process '{processName}' with PID '{process.Id}': {ex.Message}", severity);
                }
            }
        }

        public void StartBackend()
        {
            TestLogger.WriteLineT("Starting application backend", LogSeverity.Info);

            var backendFileFullName = Path.GetFullPath(Path.Combine(_beExePath, _beExeName));
            _backendProcess.StartInfo.FileName = backendFileFullName;

            try
            {
                _backendProcess.Start();
                TestLogger.WriteLineT($"Application backend process '{_beExeName}' has been started with PID '{_backendProcess.Id}'", LogSeverity.Debug);
            }
            catch (Exception ex)
            {
                TestLogger.WriteLineT($"Unable to start application backend at '{backendFileFullName}': {ex.Message}", LogSeverity.Error);
                throw;
            }            
        }

        public async Task WaitForBackedProcessAsync(int timeoutMs, int checkEveryMs)
        {
            TestLogger.WriteLineT("Waiting for application backend process to appear in system process list", LogSeverity.Trace);
            await WaitForProcessIsReady(ConfigurationProvider.BackendProcessName, timeoutMs, checkEveryMs, LogSeverity.Error);
        }

        public void StopBacked()
        {
            try
            {
                _backendProcess.Kill();
                TestLogger.WriteLineT($"Application backend process PID '{_backendProcess.Id}' has been killed", LogSeverity.Debug);
            }
            catch (Exception ex)
            {
                TestLogger.WriteLineT($"Unable to kill application backend process with PID '{_backendProcess.Id}': {ex.Message}", LogSeverity.Warn);
            }
        }

        public void MakeSureNoBackedIsRunning()
        {
            TestLogger.WriteLineT("Checking for running application backend process to kill them all", LogSeverity.Trace);
            MakeSureNoProcessIsRunning(ConfigurationProvider.BackendProcessName, LogSeverity.Warn);
        }

        public void StartWebServer()
        {
            TestLogger.WriteLineT("Starting web server", LogSeverity.Info);

            var webServerFileFullName = Path.GetFullPath(Path.Combine(_wsBatPath, _wsBatName));
            _webServerProcess.StartInfo.FileName = webServerFileFullName;
            _webServerProcess.StartInfo.Arguments = _wsWebPath;

            try
            {
                _webServerProcess.Start();
                TestLogger.WriteLineT($"Web server has been started at '{_wsWebPath}'", LogSeverity.Debug);
            }
            catch (Exception ex)
            {
                TestLogger.WriteLineT($"Unable to start web servery from executable file '{webServerFileFullName}' with argument '{_wsBatPath}': {ex.Message}", LogSeverity.Fatal);
                throw;
            }
        }

        public async Task WaitForWebServerProcessAsync(int timeoutMs, int checkEveryMs)
        {
            TestLogger.WriteLineT("Waiting for application process to appear in system process list", LogSeverity.Trace);
            await WaitForProcessIsReady(ConfigurationProvider.WebServerProcessName, timeoutMs, checkEveryMs, LogSeverity.Fatal);
        }

        public void StopWebServer()
        {
            try
            {
                _webServerProcess.Kill(); // this is not the actual web server process, but the process that started it
                Process.GetProcessesByName(ConfigurationProvider.WebServerProcessName).First().Kill(); // this is actual web server process
                TestLogger.WriteLineT($"Web server process has been killed", LogSeverity.Debug);
            }
            catch (Exception ex)
            {
                TestLogger.WriteLineT($"Unable to kill web server process: {ex.Message}", LogSeverity.Warn);
            }
        }

        public void MakeSureNoWebServerIsRunning()
        {
            TestLogger.WriteLineT("Checking for running web server process to kill them all", LogSeverity.Trace);
            MakeSureNoProcessIsRunning(ConfigurationProvider.WebServerProcessName, LogSeverity.Warn);
        }
    }
}
