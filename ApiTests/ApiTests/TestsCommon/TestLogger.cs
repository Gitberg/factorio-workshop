﻿using NUnit.Framework;
using System;
using System.Globalization;
using System.Text;

namespace ApiTests.TestsCommon
{
    public enum LogSeverity
    {
        Trace,  // very detailed logging usually used only during development
        Debug,  // information helpful to more people than just developer during problem investigation
        Info,   // generally useful information, standard level of logging when not dealing with problems or development
        Audit,  // client-server connection related information
        Warn,   // problem that does not make test stop, but is not expected and may potentially cause oddities
        Error,  // problem that blocks single test from continue and finishing successfully
        Fatal   // problem that blocks whole test run from continue, may block multiple tests from finishing successfully
    }

    public static class TestLogger
    {
        const string LogItemSeparator = " : ";
        const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff";       

        static string FormatSeverity(LogSeverity severity)
        {
            var result = new StringBuilder();

            switch(severity)
            {
                case LogSeverity.Trace: result.Append("T"); break;
                case LogSeverity.Debug: result.Append("D"); break;
                case LogSeverity.Info: result.Append("I"); break;
                case LogSeverity.Audit: result.Append("A"); break;
                case LogSeverity.Warn: result.Append("W"); break;
                case LogSeverity.Error: result.Append("E"); break;
                case LogSeverity.Fatal: result.Append("F"); break;
            }

            return result.ToString();
        }

        static void LogEntry(string message, LogSeverity severity, bool useTimeStamp)
        {
            if ((int)severity < ConfigurationProvider.LogLevel)
                return;

            var logEntry = new StringBuilder();

            if (useTimeStamp)
                logEntry.Append(DateTime.UtcNow.ToString(DateTimeFormat, CultureInfo.InvariantCulture)).Append(LogItemSeparator);

            logEntry.Append(FormatSeverity(severity)).Append(LogItemSeparator);

            logEntry.Append(message);

            TestContext.Progress.WriteLine(logEntry.ToString());
        }

        public static void WriteLine(string message, LogSeverity severity = LogSeverity.Info)
        {
            LogEntry(message, severity, false);
        }

        public static void WriteLineT(string message, LogSeverity severity = LogSeverity.Info)
        {
            LogEntry(message, severity, true);
        }
    }
}
