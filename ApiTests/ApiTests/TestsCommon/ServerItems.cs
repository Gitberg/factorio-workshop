﻿using System.Collections.Generic;
using System.Linq;

namespace ApiTests.TestsCommon
{
    public static class ResourceType
    {
        public static string None { get; } = "None";
        public static string Stone { get; } = "Stone";
        public static string IronOre { get; } = "IronOre";
        public static string CopperOre { get; } = "CopperOre";
        public static string Oil { get; } = "Oil";
        public static string Coal { get; } = "Coal";
        public static string IronPlate { get; } = "IronPlate";
        public static string Steel { get; } = "Steel";
        public static string SteelCasing { get; } = "SteelCasing";
        public static string RocketShell { get; } = "RocketShell";
        public static string CopperPlate { get; } = "CopperPlate";
        public static string Wire { get; } = "Wire";
        public static string Circuit { get; } = "Circuit";
        public static string Computer { get; } = "Computer";
        public static string Petroleum { get; } = "Petroleum";
        public static string BasicFuel { get; } = "BasicFuel";
        public static string SolidFuel { get; } = "SolidFuel";
        public static string RocketFuel { get; } = "RocketFuel";
    }

    public static class ResourceCategory
    {
        public static List<string> RawResources { get; } = new List<string>
        {
            ResourceType.Stone, ResourceType.IronOre, ResourceType.CopperOre, ResourceType.Oil, ResourceType.Coal
        };

        public static List<string> IronProducts { get; } = new List<string>
        {
            ResourceType.IronPlate, ResourceType.Steel, ResourceType.SteelCasing, ResourceType.RocketShell
        };

        public static List<string> CopperProducts { get; } = new List<string>
        {
            ResourceType.CopperPlate, ResourceType.Wire, ResourceType.Circuit, ResourceType.Computer
        };

        public static List<string> OilProducts { get; } = new List<string>
        {
            ResourceType.Petroleum, ResourceType.BasicFuel, ResourceType.SolidFuel, ResourceType.RocketFuel
        };

        public static List<string> AllResources
        {
            get
            {
                return RawResources
                    .Concat(IronProducts)
                    .Concat(CopperProducts)
                    .Concat(OilProducts)
                    .ToList();
            }
        }
    }

    public static class BuildingType
    {
        public static string None { get; } = "None";
        public static string Mine { get; } = "Mine";
        public static string IronPlateFactory { get; } = "IronPlateFactory";
        public static string SteelFactory { get; } = "SteelFactory";
        public static string SteelCasingFactory { get; } = "SteelCasingFactory";
        public static string RocketShellFactory { get; } = "RocketShellFactory";
        public static string CopperPlateFactory { get; } = "CopperPlateFactory";
        public static string WireFactory { get; } = "WireFactory";
        public static string CircuitFactory { get; } = "CircuitFactory";
        public static string ComputerFactory { get; } = "ComputerFactory";
        public static string PetroleumFactory { get; } = "PetroleumFactory";
        public static string BasicFuelFactory { get; } = "BasicFuelFactory";
        public static string SolidFuelFactory { get; } = "SolidFuelFactory";
        public static string RocketFuelFactory { get; } = "RocketFuelFactory";
    }

    public static class BuildingCategory
    {
        public static List<string> BasicBuildings { get; } = new List<string>
        {
            BuildingType.Mine
        };

        public static List<string> IronBuildings { get; } = new List<string>
        {
            BuildingType.IronPlateFactory, BuildingType.SteelFactory, BuildingType.SteelCasingFactory, BuildingType.RocketShellFactory
        };

        public static List<string> CopperBuildings { get; } = new List<string>
        {
            BuildingType.CopperPlateFactory, BuildingType.WireFactory, BuildingType.CircuitFactory, BuildingType.ComputerFactory
        };

        public static List<string> OilBuildings { get; } = new List<string>
        {
            BuildingType.PetroleumFactory, BuildingType.BasicFuelFactory, BuildingType.SolidFuelFactory, BuildingType.RocketFuelFactory
        };

        public static List<string> AllBuildings
        {
            get
            {
                return BasicBuildings
                    .Concat(IronBuildings)
                    .Concat(CopperBuildings)
                    .Concat(OilBuildings)
                    .ToList();
            }
        }
    }

    public static class ServerActions
    {
        public static string SuccessfulResponse { get; } = "ok";
        public static string DestroyBuilding { get; } = "DestroyBuilding";
        public static string Mine { get; } = "MineResource";
        public static string Build { get; } = "Build";

    }

    public static class UrlPostfixes
    {
        public static string ResourcePoolRequest { get; } = "bank";
        public static string MapRequest { get; } = "world";
        public static string MineResource { get; } = "turns";
    }

    public class ResourceItem
    {
        public string Resource { get; set; }
        public int Amount { get; set; }
    }

    public class MapTiles
    {
        public MapTile[,] Tiles { get; set; }
    }

    public class MapTile
    {
        public ResourceItem ResourceSite { get; set; }
        public string Building { get; set; }
    }

    public class ActionRequestParams
    {
        public string PlayerName { get; set; }
        public string Action { get; set; }
        public TileCoords Coordinates { get; set; }
    }

    public class ActionRequestResponse
    {
        public string Ok { get; set; }
    }

    public class TileCoords
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
