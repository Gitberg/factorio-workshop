﻿using NUnit.Framework;
using System.Threading.Tasks;

namespace ApiTests.TestsCommon
{
    public class TestBase
    {
        protected ProcessHandler ProcessHandler = new ProcessHandler();
        protected Server Server = new Server();

        [OneTimeSetUp]
        protected async Task OneTimeInitialize()
        {
            TestLogger.WriteLineT("One time initialize started", LogSeverity.Trace);

            if (ConfigurationProvider.StartWebServer)
            {
                ProcessHandler.MakeSureNoWebServerIsRunning();
                ProcessHandler.StartWebServer();
                await ProcessHandler.WaitForWebServerProcessAsync(10000, 500);
            }

            TestLogger.WriteLineT("One time initialize finished", LogSeverity.Trace);
        }

        [SetUp]
        protected async Task Initialize()
        {
            TestLogger.WriteLineT("Initialize started", LogSeverity.Trace);

            if (ConfigurationProvider.StartApplication)
            {
                ProcessHandler.MakeSureNoBackedIsRunning();
                ProcessHandler.StartBackend();
                await ProcessHandler.WaitForBackedProcessAsync(10000, 500);
            }

            await Server.WaitForServerAsync(10000, 250);

            TestLogger.WriteLineT("Initialize finished", LogSeverity.Trace);
            TestLogger.WriteLineT($"Test '{GetType().Name}' started", LogSeverity.Info);
        }

        [TearDown]
        protected void Cleanup()
        {
            TestLogger.WriteLineT($"Test '{GetType().Name}' finished", LogSeverity.Info);
            TestLogger.WriteLineT("Cleanup started", LogSeverity.Trace);

            if (ConfigurationProvider.StopApplication)
            {
                TestLogger.WriteLineT("Stopping application backend", LogSeverity.Info);
                ProcessHandler.StopBacked();
            }

            TestLogger.WriteLineT("Cleanup finished", LogSeverity.Trace);
        }

        [OneTimeTearDown]
        protected void OneTimeCleanup()
        {
            TestLogger.WriteLineT("One time cleanup started", LogSeverity.Trace);

            if (ConfigurationProvider.StopWebServer)
            {
                TestLogger.WriteLineT("Stopping web server", LogSeverity.Info);
                ProcessHandler.StopWebServer();
            }

            TestLogger.WriteLineT("One time cleanup finished", LogSeverity.Trace);
        }
    }
}
