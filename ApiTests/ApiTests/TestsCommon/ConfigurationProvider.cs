﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace ApiTests.TestsCommon
{
    public static class ConfigurationProvider
    {
        const char ListItemSeparator = ';';

        // connet settings
        public static string ServerPrefix { get; } = ConfigurationManager.AppSettings["ServerPrefix"];
        public static string ServerUrl { get; } = ConfigurationManager.AppSettings["ServerUrl"];
        public static string ServerPort { get; } = ConfigurationManager.AppSettings["ServerPort"];
        public static string RequestPort { get; } = ConfigurationManager.AppSettings["RequestPort"];

        // environment settings
        public static int LogLevel
        {
            get
            {
                switch(ConfigurationManager.AppSettings["LogLevel"])
                {
                    case nameof(LogSeverity.Trace):
                        return 0;
                    case nameof(LogSeverity.Debug):
                        return 1;
                    case nameof(LogSeverity.Info):
                        return 2;
                    case nameof(LogSeverity.Audit):
                        return 3;
                    case nameof(LogSeverity.Warn):
                        return 4;
                    case nameof(LogSeverity.Error):
                        return 5;
                    case nameof(LogSeverity.Fatal):
                        return 6;
                    default:
                        return 2;
                }
            }
        }
        public static bool StartWebServer { get; } = bool.Parse(ConfigurationManager.AppSettings["StartWebServer"]);
        public static bool StartApplication { get; } = bool.Parse(ConfigurationManager.AppSettings["StartApplication"]);
        public static bool StopApplication { get; } = bool.Parse(ConfigurationManager.AppSettings["StopApplication"]);
        public static bool StopWebServer { get; } = bool.Parse(ConfigurationManager.AppSettings["StopWebServer"]);
        public static string BackendExecutableFilePath { get; } = ConfigurationManager.AppSettings["BackendExecutableFilePath"];
        public static string BackendExecutableFileName { get; } = ConfigurationManager.AppSettings["BackendExecutableFileName"];
        public static string BackendProcessName { get; } = ConfigurationManager.AppSettings["BackendProcessName"];
        public static string WebServerExecutablePath { get; } = ConfigurationManager.AppSettings["WebServerExecutablePath"];
        public static string WebServerExecutableFileName { get; } = ConfigurationManager.AppSettings["WebServerExecutableFileName"];
        public static string WebServerLocalhostPath { get; } = ConfigurationManager.AppSettings["WebServerLocalhostPath"];        
        public static string WebServerProcessName { get; } = ConfigurationManager.AppSettings["WebServerProcessName"];

        // game settings
        public static int TickTimeMs { get; } = int.Parse(ConfigurationManager.AppSettings["TickTimeMs"]);
        public static int AskForChangeAfterMs { get; } = int.Parse(ConfigurationManager.AppSettings["AskForChangeAfterMs"]);
        public static List<string> PlayerNames { get; } = ConfigurationManager.AppSettings["PlayerNames"].Split(ListItemSeparator).ToList();
        public static int MapWidth { get; } = int.Parse(ConfigurationManager.AppSettings["MapWidth"]);
        public static int MapHeight { get; } = int.Parse(ConfigurationManager.AppSettings["MapHeight"]);        
        public static int ResourceGainFromMineTick { get; } = int.Parse(ConfigurationManager.AppSettings["ResourceGainFromMineTick"]);
        public static int StoneNeededForBuilding { get; } = int.Parse(ConfigurationManager.AppSettings["StoneNeededForBuilding"]);
        public static int VictoryTopResourceAmount { get; } = int.Parse(ConfigurationManager.AppSettings["VictoryTopResourceAmount"]);
        public static int LowerResourcesNeededForHigher { get; } = int.Parse(ConfigurationManager.AppSettings["LowerResourcesNeededForHigher"]);
        public static int CoalNeededForResourceTransformation { get; } = int.Parse(ConfigurationManager.AppSettings["CoalNeededForResourceTransformation"]);
        public static int FactoryLevels { get; } = int.Parse(ConfigurationManager.AppSettings["FactoryLevels"]);
        public static int NumberOfProductionResources { get; } = int.Parse(ConfigurationManager.AppSettings["NumberOfProductionResources"]);
    }
}
