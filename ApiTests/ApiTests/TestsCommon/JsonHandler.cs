﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiTests.TestsCommon
{
    public static class JsonHandler
    {
        public static T Deserialize<T>(string json)
        {
            TestLogger.WriteLineT($"Deserializing payload '{json}'", LogSeverity.Trace);

            try
            {
                return JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception ex)
            {
                TestLogger.WriteLineT($"Cannot deserialize '{json}' because: {ex.Message}", LogSeverity.Warn);
                return default;
            }
        }

        public static string Serialize<T>(T data)
        {
            TestLogger.WriteLineT($"Serializing data object {data.GetType().Name}", LogSeverity.Trace);
            
            try
            {
                return JsonConvert.SerializeObject(data);
            }
            catch (Exception ex)
            {
                TestLogger.WriteLineT($"Cannot serialize '{data.GetType().Name}' because: {ex.Message}", LogSeverity.Warn);
                return null;
            }
        }
    }
}
