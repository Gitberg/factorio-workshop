﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net.Http;

namespace ApiTests.TestsCommon
{
    public class Client
    {
        Server _server = new Server();
        string _playerName;

        public Client(string playerName)
        {
            if (!ConfigurationProvider.PlayerNames.Any(p => p.ToUpperInvariant() == playerName.ToUpperInvariant()))
            {
                var errorMessage = $"Player name '{playerName}' not found in list of allowed players: '{ConfigurationProvider.PlayerNames}'";
                TestLogger.WriteLineT(errorMessage, LogSeverity.Error);
                throw new ArgumentOutOfRangeException(errorMessage);
            }

            _playerName = playerName;
        }

        async Task<T> GetDataAsync<T>(string urlPostfix)
        {
            using (var response = await _server.SendGetRequest(urlPostfix))
            {
                if (response == null)
                {
                    TestLogger.WriteLineT($"No response for GET request '{urlPostfix}'", LogSeverity.Warn);
                    return default;
                }

                if (!response.IsSuccessStatusCode)
                {
                    TestLogger.WriteLineT($"Status code for GET request '{urlPostfix}' was '{response.StatusCode}'", LogSeverity.Warn);
                    return default;
                }

                var responsePayload = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                return JsonHandler.Deserialize<T>(responsePayload);
            }
        }

        public async Task<List<ResourceItem>> GetResourcesAsync()
        {
            TestLogger.WriteLineT("Refreshing resource pool", LogSeverity.Trace);
            return await GetDataAsync<List<ResourceItem>>(UrlPostfixes.ResourcePoolRequest);
        }

        public async Task<MapTiles> GetMapAsync()
        {
            TestLogger.WriteLineT("Refreshing world map", LogSeverity.Trace);
            return await GetDataAsync<MapTiles>(UrlPostfixes.MapRequest);
        }

        public async Task<TileCoords> TryFindFirstFreeResourceAsync(string resource)
        {
            TestLogger.WriteLineT($"Trying to find first free resource of '{resource}'", LogSeverity.Debug);
            var map = await GetMapAsync();

            for (var col = 0; col < map.Tiles.GetLength(0); col++)
            {
                for (var row = 0; row < map.Tiles.GetLength(1); row++)
                {
                    var tile = map.Tiles[col, row];
                    if (tile.ResourceSite.Resource == resource)
                    {
                        if (tile.Building == BuildingType.None)
                        {
                            TestLogger.WriteLineT($"Free resource of '{resource}' found at ['{row}','{col}']", LogSeverity.Trace);
                            return new TileCoords() { X = row, Y = col };
                        }
                    }
                }
            }

            TestLogger.WriteLineT($"Free resource of '{resource}' not found", LogSeverity.Warn);
            return null;
        }

        public async Task<TileCoords> TryFindRandomFreeResourceAsync(string resource)
        {
            TestLogger.WriteLineT($"Trying to find random free resource of '{resource}'", LogSeverity.Debug);
            var map = await GetMapAsync();
            var listOfValidTiles = new List<(MapTile tile, int x, int y)>();

            for (var col = 0; col < map.Tiles.GetLength(0); col++)
            {
                for (var row = 0; row < map.Tiles.GetLength(1); row++)
                {
                    if (map.Tiles[col, row].ResourceSite.Resource == resource && map.Tiles[col, row].Building == BuildingType.None)
                    {
                        listOfValidTiles.Add((map.Tiles[col, row], row, col));
                    }
                }
            }

            if (listOfValidTiles.Count() < 1)
            {
                TestLogger.WriteLineT($"No free resource tile of '{resource}' found", LogSeverity.Warn);
                return null;
            }

            var randomTile = listOfValidTiles[new Random().Next(listOfValidTiles.Count())];

            return new TileCoords() { X = randomTile.x, Y = randomTile.y };
        }

        async Task<T2> PostDataAsync<T1, T2>(T1 request)
        {
            var paramsJson = JsonHandler.Serialize<T1>(request);

            using (var response = await _server.SendPostRequest(UrlPostfixes.MineResource, new StringContent(paramsJson)))
            {
                if (response == null)
                {
                    TestLogger.WriteLineT($"No response for POST request '{request.GetType().Name}'", LogSeverity.Warn);
                    return default;
                }

                if (!response.IsSuccessStatusCode)
                {
                    TestLogger.WriteLineT($"Status code for POST request '{request.GetType().Name}' was '{response.StatusCode}'", LogSeverity.Warn);
                    return default;
                }

                var responsePayload = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                return JsonHandler.Deserialize<T2>(responsePayload);
            }
        }

        async Task<bool> TryDoAction(ActionRequestParams requestParams)
        {
            TestLogger.WriteLineT($"Sending request for '{requestParams.Action}' at '[{requestParams.Coordinates.X},{requestParams.Coordinates.Y}]'", LogSeverity.Trace);

            var responsePayload = await PostDataAsync<ActionRequestParams, ActionRequestResponse>(requestParams);

            return responsePayload.Ok == ServerActions.SuccessfulResponse;
        }

        public async Task<bool> TryMineAsync(int x, int y)
        {
            TestLogger.WriteLineT($"Trying to mine resource from ['{x}','{y}']", LogSeverity.Debug);

            if (!CoordsAreValid(x, y))
                return false;

            var requestParams = new ActionRequestParams()
            {
                PlayerName = _playerName,
                Action = ServerActions.Mine,
                Coordinates = new TileCoords()
                {
                    X = x,
                    Y = y
                }
            };

            return await TryDoAction(requestParams);
        }

        public async Task<bool> TryBuildAsync(string building, int x, int y)
        {
            TestLogger.WriteLineT($"Building '{building}' at '[{x},{y}]'", LogSeverity.Debug);

            if (!CoordsAreValid(x, y))
                return false;

            var requestParams = new ActionRequestParams()
            {
                PlayerName = _playerName,
                Action = ServerActions.Build + building,
                Coordinates = new TileCoords()
                {
                    X = x,
                    Y = y
                }
            };

            return await TryDoAction(requestParams);
        }

        public async Task<bool> TryDestroyAsync(int x, int y)
        {
            TestLogger.WriteLineT($"Destroying building at '[{x},{y}]'", LogSeverity.Debug);

            if (!CoordsAreValid(x, y))
                return false;

            var requestParams = new ActionRequestParams()
            {
                PlayerName = _playerName,
                Action = ServerActions.DestroyBuilding,
                Coordinates = new TileCoords()
                {
                    X = x,
                    Y = y
                }
            };

            return await TryDoAction(requestParams);
        }

        public async Task<bool> WaitForResourceAsync(string resource, int amount, int timeoutMs)
        {
            TestLogger.WriteLineT($"Waiting for '{amount}' of '{resource}' with timeout '{timeoutMs}' ms", LogSeverity.Debug);

            var currentTime = DateTime.UtcNow;
            var timeoutTime = DateTime.UtcNow.AddMilliseconds(timeoutMs);           

            while (currentTime <= timeoutTime)
            {
                var resourcePool = await GetResourcesAsync();
                var result = resourcePool.FirstOrDefault(r => r.Resource == resource);

                if (result == null)
                {
                    TestLogger.WriteLineT($"Resource with name '{resource}' is not present in resource pool", LogSeverity.Warn);
                    return false;
                }

                if (result.Amount >= amount)
                {
                    return true;
                }

                TestLogger.WriteLineT($"Resource with name '{resource}' is present with count '{result.Amount}' but waitingn for '{amount}'", LogSeverity.Trace);

                await Task.Delay(TimeSpan.FromMilliseconds(ConfigurationProvider.AskForChangeAfterMs));
                currentTime = DateTime.UtcNow;
            }

            TestLogger.WriteLineT($"Resource with name '{resource}' is not in resource pool in amount '{amount}' after timeout '{timeoutMs}' ms", LogSeverity.Warn);
            return false;
        }

        public async Task<bool> WaitForBuildingAsync(string building, int x, int y, int timeoutMs)
        {
            TestLogger.WriteLineT($"Waiting for building '{building}' to appear at '[{x},{y}]' with timeout '{timeoutMs}' ms", LogSeverity.Debug);

            var currentTime = DateTime.UtcNow;
            var timeoutTime = DateTime.UtcNow.AddMilliseconds(timeoutMs);

            while (currentTime <= timeoutTime)
            {
                var map = await GetMapAsync();

                if (map.Tiles[y, x].Building == building)
                {
                    TestLogger.WriteLineT($"Building '{building}' found at '[{x},{y}]'", LogSeverity.Trace);
                    return true;
                }
                                                  
                await Task.Delay(TimeSpan.FromMilliseconds(ConfigurationProvider.AskForChangeAfterMs));
                currentTime = DateTime.UtcNow;
            }

            TestLogger.WriteLineT($"Building '{building}' is not present at '[{x},{y}]' after timeout '{timeoutMs}' ms", LogSeverity.Warn);
            return false;
        }

        bool CoordsAreValid(int x, int y)
        {
            TestLogger.WriteLineT($"Testing coordinates '[{x},{y}]'", LogSeverity.Trace);

            if (x < 0 || x > ConfigurationProvider.MapWidth - 1)
            {
                TestLogger.WriteLineT($"Attempt to mine from outside of map dimensions: x = '{x}', map width = '{ConfigurationProvider.MapWidth}'", LogSeverity.Warn);
                return false;
            }

            if (y < 0 || y > ConfigurationProvider.MapHeight - 1)
            {
                TestLogger.WriteLineT($"Attempt to mine from outside of map dimensions: y = '{y}', map height = '{ConfigurationProvider.MapHeight}'", LogSeverity.Warn);
                return false;
            }

            TestLogger.WriteLineT($"Coordinates '[{x},{y}]' are valid", LogSeverity.Trace);
            return true;
        }

        public async Task<int> GetNumberOfResourceDepositsOnMapAsync(string resource)
        {
            TestLogger.WriteLineT($"Getting number of '{resource}' resource on map", LogSeverity.Debug);
            var map = await GetMapAsync();
            var number = 0;

            for (var col = 0; col < map.Tiles.GetLength(0); col++)
            {
                for (var row = 0; row < map.Tiles.GetLength(1); row++)
                {
                    var tile = map.Tiles[col, row];
                    if (tile.ResourceSite.Resource == resource)
                    {
                        number++;
                        TestLogger.WriteLineT($"Resource deposit of '{resource}' found at ['{row}','{col}']", LogSeverity.Trace);
                    }
                }
            }

            return number;
        }
    }
}