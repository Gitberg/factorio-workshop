﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ApiTests.TestsCommon
{
    public class Server
    {
        readonly string _localhostUrl;
        readonly string _requestUrl;

        public Server()
        {
            _localhostUrl =
            $"{ConfigurationProvider.ServerPrefix}" +
            $"://{ConfigurationProvider.ServerUrl}" +
            $":{ConfigurationProvider.ServerPort}";

            _requestUrl =
            $"{ConfigurationProvider.ServerPrefix}" +
            $"://{ConfigurationProvider.ServerUrl}" +
            $":{ConfigurationProvider.RequestPort}";
        }

        public async Task WaitForServerAsync(int timeoutMs, int delayBetweenChecksMs)
        {
            TestLogger.WriteLineT($"Waiting until server '{_localhostUrl}' can be reached by HTTP client", LogSeverity.Trace);

            var currentTime = DateTime.UtcNow;
            var timeoutTime = DateTime.UtcNow.AddMilliseconds(timeoutMs);

            while (currentTime <= timeoutTime)
            {
                if (await CanReachServerAsync())
                    return;

                await Task.Delay(TimeSpan.FromMilliseconds(delayBetweenChecksMs));
                currentTime = DateTime.UtcNow;
            }

            var errorMessage = $"Unable to connect with application '{_localhostUrl}' in given time limit '{timeoutMs}' ms";
            TestLogger.WriteLineT(errorMessage, LogSeverity.Fatal);
            throw new Exception(errorMessage);
        }

        async Task<bool> CanReachServerAsync()
        {
            using (var httpClient = new HttpClient())
            {
                HttpResponseMessage response;

                try
                {
                    response = await httpClient.GetAsync(_localhostUrl).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    TestLogger.WriteLineT($"Cannot connect to '{_localhostUrl}' because '{ex.Message}'", LogSeverity.Trace);
                    return false;
                }

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TestLogger.WriteLineT($"Http response from server '{_localhostUrl}' should be '{nameof(HttpStatusCode.OK)}' but received '{response.StatusCode}'", LogSeverity.Trace);
                    return false;
                }

                return true;
            }
        }

        public async Task<HttpResponseMessage> SendGetRequest(string urlPostfix)
        {
            var callUrl = $"{_requestUrl}/{urlPostfix}";

            TestLogger.WriteLineT($"Sending GET request to '{callUrl}'", LogSeverity.Trace);

            using (var httpClient = new HttpClient())
            {
                try
                {
                    return await httpClient.GetAsync(callUrl).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    TestLogger.WriteLineT($"Cannot call '{callUrl}' because: '{ex.Message}'", LogSeverity.Warn);
                    return null;
                }
            }
        }

        public async Task<HttpResponseMessage> SendPostRequest(string urlPostfix, StringContent parameters)
        {
            var callUrl = $"{_requestUrl}/{urlPostfix}";

            TestLogger.WriteLineT($"Sending POST request to '{callUrl}'", LogSeverity.Trace);

            using (var httpClient = new HttpClient())
            {
                try
                {
                    return await httpClient.PostAsync(callUrl, parameters).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    TestLogger.WriteLineT($"Cannot call '{callUrl}' because: '{ex.Message}'", LogSeverity.Warn);
                    return null;
                }
            }
        }
    }
}
